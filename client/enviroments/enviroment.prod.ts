export const environment = {
  production: false,
  apiUrl: 'https://majestic-yosemite-19251.herokuapp.com/api/',
  usersUrl: 'https://majestic-yosemite-19251.herokuapp.com/users/',
};