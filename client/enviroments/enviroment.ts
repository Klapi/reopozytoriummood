export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000/api/',
  usersUrl: 'http://localhost:3000/users/',
};