export class Recipe{
    title: string;
    description: string;
    time: string;
    ingredients:[{
        name: string,
        quantity: string,
        unit: string
    }];
    preparing: string;
    image: string;
}