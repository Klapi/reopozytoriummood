"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var recipe_service_1 = require("../services/recipe.service");
var router_1 = require("@angular/router");
var contentArea = (function () {
    function contentArea(recipeService, router) {
        var _this = this;
        this.recipeService = recipeService;
        this.router = router;
        console.log(router.url);
        console.log(router.url);
        console.log(router.url);
        console.log(router.url);
        console.log(router.url);
        this.recipeService.getRecipes()
            .subscribe(function (recipes) {
            _this.recipes = recipes;
            console.log(recipes);
        });
    }
    contentArea.prototype.onSelect = function (recipe) {
        console.log(recipe);
        this.router.navigate(['/recipe', recipe._id]);
    };
    return contentArea;
}());
contentArea = __decorate([
    core_1.Component({
        moduleId: module.id,
        //selector: 'contentArea',
        templateUrl: 'contentArea.html',
        styleUrls: ['../styles.css']
    }),
    __metadata("design:paramtypes", [recipe_service_1.RecipeService, router_1.Router])
], contentArea);
exports.contentArea = contentArea;
//# sourceMappingURL=app.contentAreaComponent.js.map