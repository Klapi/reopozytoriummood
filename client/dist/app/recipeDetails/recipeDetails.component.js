"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var recipe_service_1 = require("../services/recipe.service");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var recipeDetails = (function () {
    function recipeDetails(recipeService, activatedRoute, location, router) {
        var _this = this;
        this.recipeService = recipeService;
        this.activatedRoute = activatedRoute;
        this.location = location;
        this.router = router;
        console.log("recipeDetails");
        console.log(router.url);
        this.router.events
            .subscribe(function (url) {
            var _ruta = "";
            url.url.split("/").forEach(function (element) {
                if (element !== "" && _ruta === "")
                    _ruta = "/" + element;
            });
            console.log("route: " + _ruta); //<<<---- Root path
            console.log("to URL:" + url.url); //<<<---- Destination URL 
            console.log("from URL:" + _this.router.url); //<<<---- Current URL
        });
    }
    recipeDetails.prototype.ngOnInit = function () {
        // subscribe to router event
        var _this = this;
        var _id = this.activatedRoute.snapshot.params['id'];
        this.recID = _id;
        console.log(this.recID);
        this.recipeService.getRecipe(this.recID)
            .subscribe(function (recipe) {
            _this.recipe = recipe;
            console.log(recipe);
        });
    };
    ;
    return recipeDetails;
}());
recipeDetails = __decorate([
    core_1.Component({
        moduleId: module.id,
        //selector: 'contentArea',
        templateUrl: 'recipeDetails.html',
        styleUrls: ['stylesRecipe.css']
    }),
    __metadata("design:paramtypes", [recipe_service_1.RecipeService, router_1.ActivatedRoute, common_1.Location, router_1.Router])
], recipeDetails);
exports.recipeDetails = recipeDetails;
//# sourceMappingURL=recipeDetails.component.js.map