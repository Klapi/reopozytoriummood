import { Component, OnInit, } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RecipeService } from '../services/recipe.service';
import { Recipe } from '../../Recipe';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

@Component({
    moduleId: module.id.replace("/dist/app/", "/"),
    templateUrl: 'recipeDetails.html',
    styleUrls: ['stylesRecipe.css']
})
export class recipeDetails implements OnInit {
    recipe: Recipe;
    id: string[];

    constructor(private recipeService: RecipeService, private activatedRoute: ActivatedRoute, private location: Location, private router: Router) {
        console.log("recipeDetails");
    }

    ngOnInit() {
        // subscribe to router event
        const token = localStorage.getItem('id_token');
        if (token == null) {
            this.router.navigate(['login']);
        }
        var res = this.router.url;
        this.id = res.split("/");
        this.recipeService.getRecipe(this.id[this.id.length - 1])
            .subscribe(recipe => {
                this.recipe = recipe;
                console.log(recipe);
            });
    };
}
