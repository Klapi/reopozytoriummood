import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { FlashMessagesModule } from 'angular2-flash-messages';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { navComponent } from './nav/app.nav.component';
import { addRecipe } from './components/recipes/add/addRecipe.components';
import { recipeList } from './recipeList/app.recipeList.component';
import { recipeDetails } from './recipeDetails/recipeDetails.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { routing } from './app.routing';

import { ValidateService } from './services/validate.service';
import { AuthService } from './services/auth.service'
//dsds

//const appRoutes: Routes = [{ path: 'addRecipe', component: RecipesComponent }, { path: '', component: contentArea }]



@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        routing,
        FlashMessagesModule],
    declarations: [
        AppComponent,
        addRecipe,
        navComponent,
        recipeList,
        recipeDetails,
        LoginComponent,
        RegisterComponent],
    providers: [
        ValidateService,
        AuthService],
    bootstrap: [AppComponent]
})
export class AppModule { }