import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class RecipeService {
    constructor(private http: Http) {
        console.log('Recipe Service Initialized...');
    }

    getRecipes(user) {
        let headers = new Headers();
        const token = localStorage.getItem('id_token');
        headers.append('Authorization', token);
        return this.http.get('/api/recipes/' + user, { headers: headers })
            .map(res => res.json());
    }

    addRecipe(newRecipe) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post('/api/recipe', JSON.stringify(newRecipe), { headers: headers })
            .map(res => res.json);
    }

    getRecipe(id) {
        let headers = new Headers();
        const token = localStorage.getItem('id_token');
        headers.append('Authorization', token);
        return this.http.get('/api/recipe/' + id, { headers: headers })
            .map(res => res.json());
    }


}