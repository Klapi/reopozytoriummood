import { Routes, RouterModule } from '@angular/router';
import { recipeDetails } from "./recipeDetails/recipeDetails.component";
import { recipeList } from "./recipeList/app.recipeList.component";
import { addRecipe } from "./components/recipes/add/addRecipe.components";
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

export const routes: Routes = [
    { path: 'addRecipe', component: addRecipe },
    { path: '', component: recipeList },
    { path: 'recipe/:id', component: recipeDetails },
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
];

export const routing = RouterModule.forRoot(routes, { useHash: true });