import { Component } from '@angular/core';
import { RecipeService } from './services/recipe.service';
import { navComponent } from './nav/app.nav.component';
import { recipeList } from './recipeList/app.recipeList.component';
import { addRecipe } from './components/recipes/add/addRecipe.components';

@Component({
  moduleId: module.id.replace("/dist/app/", "/"),
  selector: 'my-app',
  templateUrl: 'app.component.html',
  providers: [RecipeService]
})
export class AppComponent { }