import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RecipeService } from '../services/recipe.service';
import { Recipe } from '../../Recipe';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
    moduleId: module.id.replace("/dist/app/", "/"),
    //selector: 'contentArea',
    templateUrl: 'recipeList.html',
    styleUrls: ['../styles.css']
})
export class recipeList implements OnInit {
    recipes: Recipe[];
    constructor(
        private recipeService: RecipeService,
        private router: Router,
        private flashMessage: FlashMessagesService) {


    }

    onSelect(recipe) {
        console.log(recipe);
        this.router.navigate(['/recipe', recipe._id]);
    }


    ngOnInit() {
        const user = localStorage.getItem('user');
        const userParse = JSON.parse(user);
        const token = localStorage.getItem('id_token');
        if (token == null) {
            this.flashMessage.show('Please log-in first', { cssClass: 'alert-danger', timeout: 3000 });
            this.router.navigate(['login']);
        }
        else {
            this.recipeService.getRecipes(userParse.id)
                .subscribe(recipes => {
                    this.recipes = recipes;
                    console.log(recipes);
                })
        };
    }
}