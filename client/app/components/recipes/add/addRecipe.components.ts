import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../../services/recipe.service';
import { Recipe } from '../../../../Recipe';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
    moduleId: module.id.replace("/dist/app/", "/"),
    //selector: 'recipes',
    templateUrl: 'addRecipe.components.html'
})
export class addRecipe implements OnInit {
    title: string;
    description: string;
    time: string;
    name: string;
    quantity: string;
    unit: string;
    preparing: any;
    image: string;
    ingredients: any;
    ingredient: any;
    userParse: any;
    step: string;

    constructor(
        private recipeService: RecipeService,
        private router: Router,
        private flashMessage: FlashMessagesService) {
        this.preparing = [];
        this.ingredients = [];
    }

    addIng() {
        this.ingredient = {
            name: this.name,
            quantity: this.quantity,
            unit: this.unit
        }
        this.ingredients.push(this.ingredient);
        this.name = '';
        this.quantity = '';
        this.unit = '';
    }

    addStep() {
        this.preparing.push(this.step);
        this.step = '';
    }

    deleteIng(index) {
        this.ingredients.splice(index, 1);
    }

    deleteStep(index) {
        this.preparing.splice(index, 1);
    }

    addRecipe() {
        let newRecipe = {
            title: this.title,
            description: this.description,
            time: this.time,
            ingredients: this.ingredients,
            image: this.image,
            preparing: this.preparing,
            author: this.userParse.id
        }
        this.recipeService.addRecipe(newRecipe)
            .subscribe(recipe => {
                this.title = '';
                this.description = '';
                this.time = '';
                this.image = '';
                this.preparing = '';
                //for na all ingredients
                this.ingredients = [];
                this.step = '';

            });
    }

    ngOnInit() {
        const user = localStorage.getItem('user');
        this.userParse = JSON.parse(user);
        const token = localStorage.getItem('id_token');
        console.log(token);
        if (token == null) {
            this.flashMessage.show('Please log-in first', { cssClass: 'alert-danger', timeout: 3000 });
            this.router.navigate(['login']);
        }
    }
}