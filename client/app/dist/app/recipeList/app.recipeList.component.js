"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var recipe_service_1 = require('../services/recipe.service');
var router_1 = require('@angular/router');
var angular2_flash_messages_1 = require('angular2-flash-messages');
var recipeList = (function () {
    function recipeList(recipeService, router, flashMessage) {
        this.recipeService = recipeService;
        this.router = router;
        this.flashMessage = flashMessage;
    }
    recipeList.prototype.onSelect = function (recipe) {
        console.log(recipe);
        this.router.navigate(['/recipe', recipe._id]);
    };
    recipeList.prototype.ngOnInit = function () {
        var _this = this;
        var user = localStorage.getItem('user');
        var userParse = JSON.parse(user);
        var token = localStorage.getItem('id_token');
        if (token == null) {
            this.flashMessage.show('Please log-in first', { cssClass: 'alert-danger', timeout: 3000 });
            this.router.navigate(['login']);
        }
        else {
            this.recipeService.getRecipes(userParse.id)
                .subscribe(function (recipes) {
                _this.recipes = recipes;
                console.log(recipes);
            });
        }
        ;
    };
    recipeList = __decorate([
        core_1.Component({
            moduleId: module.id.replace("/dist/app/", "/"),
            //selector: 'contentArea',
            templateUrl: 'recipeList.html',
            styleUrls: ['../styles.css']
        }), 
        __metadata('design:paramtypes', [recipe_service_1.RecipeService, router_1.Router, angular2_flash_messages_1.FlashMessagesService])
    ], recipeList);
    return recipeList;
}());
exports.recipeList = recipeList;
//# sourceMappingURL=app.recipeList.component.js.map