"use strict";
var router_1 = require('@angular/router');
var recipeDetails_component_1 = require("./recipeDetails/recipeDetails.component");
var app_recipeList_component_1 = require("./recipeList/app.recipeList.component");
var addRecipe_components_1 = require("./components/recipes/add/addRecipe.components");
var login_component_1 = require('./components/login/login.component');
var register_component_1 = require('./components/register/register.component');
exports.routes = [
    { path: 'addRecipe', component: addRecipe_components_1.addRecipe },
    { path: '', component: app_recipeList_component_1.recipeList },
    { path: 'recipe/:id', component: recipeDetails_component_1.recipeDetails },
    { path: 'register', component: register_component_1.RegisterComponent },
    { path: 'login', component: login_component_1.LoginComponent },
];
exports.routing = router_1.RouterModule.forRoot(exports.routes, { useHash: true });
//# sourceMappingURL=app.routing.js.map