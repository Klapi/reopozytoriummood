"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var http_1 = require('@angular/http');
var angular2_flash_messages_1 = require('angular2-flash-messages');
var app_component_1 = require('./app.component');
var forms_1 = require('@angular/forms');
var app_nav_component_1 = require('./nav/app.nav.component');
var addRecipe_components_1 = require('./components/recipes/add/addRecipe.components');
var app_recipeList_component_1 = require('./recipeList/app.recipeList.component');
var recipeDetails_component_1 = require('./recipeDetails/recipeDetails.component');
var login_component_1 = require('./components/login/login.component');
var register_component_1 = require('./components/register/register.component');
var app_routing_1 = require('./app.routing');
var validate_service_1 = require('./services/validate.service');
var auth_service_1 = require('./services/auth.service');
//dsds
//const appRoutes: Routes = [{ path: 'addRecipe', component: RecipesComponent }, { path: '', component: contentArea }]
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                forms_1.FormsModule,
                app_routing_1.routing,
                angular2_flash_messages_1.FlashMessagesModule],
            declarations: [
                app_component_1.AppComponent,
                addRecipe_components_1.addRecipe,
                app_nav_component_1.navComponent,
                app_recipeList_component_1.recipeList,
                recipeDetails_component_1.recipeDetails,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent],
            providers: [
                validate_service_1.ValidateService,
                auth_service_1.AuthService],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map