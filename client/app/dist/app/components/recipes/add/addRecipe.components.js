"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var recipe_service_1 = require('../../../services/recipe.service');
var router_1 = require('@angular/router');
var angular2_flash_messages_1 = require('angular2-flash-messages');
var addRecipe = (function () {
    function addRecipe(recipeService, router, flashMessage) {
        this.recipeService = recipeService;
        this.router = router;
        this.flashMessage = flashMessage;
        this.preparing = [];
        this.ingredients = [];
    }
    addRecipe.prototype.addIng = function () {
        this.ingredient = {
            name: this.name,
            quantity: this.quantity,
            unit: this.unit
        };
        this.ingredients.push(this.ingredient);
        this.name = '';
        this.quantity = '';
        this.unit = '';
    };
    addRecipe.prototype.addStep = function () {
        this.preparing.push(this.step);
        this.step = '';
    };
    addRecipe.prototype.deleteIng = function (index) {
        this.ingredients.splice(index, 1);
    };
    addRecipe.prototype.deleteStep = function (index) {
        this.preparing.splice(index, 1);
    };
    addRecipe.prototype.addRecipe = function () {
        var _this = this;
        var newRecipe = {
            title: this.title,
            description: this.description,
            time: this.time,
            ingredients: this.ingredients,
            image: this.image,
            preparing: this.preparing,
            author: this.userParse.id
        };
        this.recipeService.addRecipe(newRecipe)
            .subscribe(function (recipe) {
            _this.title = '';
            _this.description = '';
            _this.time = '';
            _this.image = '';
            _this.preparing = '';
            //for na all ingredients
            _this.ingredients = [];
            _this.step = '';
        });
    };
    addRecipe.prototype.ngOnInit = function () {
        var user = localStorage.getItem('user');
        this.userParse = JSON.parse(user);
        var token = localStorage.getItem('id_token');
        console.log(token);
        if (token == null) {
            this.flashMessage.show('Please log-in first', { cssClass: 'alert-danger', timeout: 3000 });
            this.router.navigate(['login']);
        }
    };
    addRecipe = __decorate([
        core_1.Component({
            moduleId: module.id.replace("/dist/app/", "/"),
            //selector: 'recipes',
            templateUrl: 'addRecipe.components.html'
        }), 
        __metadata('design:paramtypes', [recipe_service_1.RecipeService, router_1.Router, angular2_flash_messages_1.FlashMessagesService])
    ], addRecipe);
    return addRecipe;
}());
exports.addRecipe = addRecipe;
//# sourceMappingURL=addRecipe.components.js.map