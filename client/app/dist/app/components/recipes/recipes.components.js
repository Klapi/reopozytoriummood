"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var recipe_service_1 = require("../../services/recipe.service");
var RecipesComponent = (function () {
    function RecipesComponent(recipeService) {
        var _this = this;
        this.recipeService = recipeService;
        this.recipeService.getRecipes()
            .subscribe(function (recipes) {
            _this.recipes = recipes;
        });
    }
    RecipesComponent.prototype.addRecipe = function (event) {
        var _this = this;
        event.preventDefault();
        var newRecipe = {
            title: this.title,
            description: this.description,
            time: this.time,
            ingredients: this.ingredients,
            image: this.image,
            preparing: this.preparing
        };
        this.recipeService.addRecipe(newRecipe)
            .subscribe(function (recipe) {
            _this.recipes.push(newRecipe);
            _this.title = '';
            _this.description = '';
            _this.time = '';
            _this.ingredients = '';
            _this.image = '';
            _this.preparing = '';
        });
    };
    return RecipesComponent;
}());
RecipesComponent = __decorate([
    core_1.Component({
        moduleId: module.id.replace("/dist/app/", "/"),
        //selector: 'recipes',
        templateUrl: 'recipes.components.html'
    }),
    __metadata("design:paramtypes", [recipe_service_1.RecipeService])
], RecipesComponent);
exports.RecipesComponent = RecipesComponent;
//# sourceMappingURL=recipes.components.js.map