"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var auth_service_1 = require('../services/auth.service');
var router_1 = require('@angular/router');
var angular2_flash_messages_1 = require('angular2-flash-messages');
var navComponent = (function () {
    function navComponent(authService, router, flashMessage) {
        this.authService = authService;
        this.router = router;
        this.flashMessage = flashMessage;
        console.log(this.boolLog);
    }
    navComponent.prototype.isLoggedIn = function () {
        return this.authService.isLoggedIn();
    };
    navComponent.prototype.ngOnInit = function () {
        var log = localStorage.getItem('id_token');
        console.log(log);
        this.boolLog = 1;
        if (log == null) {
            this.boolLog = null;
        }
    };
    navComponent.prototype.onLogoutClick = function () {
        this.authService.logout();
        this.flashMessage.show('You are logged out', {
            cssClass: 'alert-success',
            timeout: 3000
        });
        this.router.navigate(['/login']);
        return false;
    };
    navComponent = __decorate([
        core_1.Component({
            moduleId: module.id.replace("/dist/app/", "/"),
            selector: 'navi',
            templateUrl: 'nav.html',
            styleUrls: ['../styles.css']
        }), 
        __metadata('design:paramtypes', [auth_service_1.AuthService, router_1.Router, angular2_flash_messages_1.FlashMessagesService])
    ], navComponent);
    return navComponent;
}());
exports.navComponent = navComponent;
//# sourceMappingURL=app.nav.component.js.map