"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var recipeDetails_component_1 = require("./recipeDetails/recipeDetails.component");
var app_contentArea_component_1 = require("./contentArea/app.contentArea.component");
var recipes_components_1 = require("./components/recipes/recipes.components");
exports.routes = [
    { path: 'addRecipe', component: recipes_components_1.RecipesComponent },
    { path: '', component: app_contentArea_component_1.contentArea },
    { path: 'recipe/:id', component: recipeDetails_component_1.recipeDetails }
];
exports.routing = router_1.RouterModule.forRoot(exports.routes, { useHash: true });
//# sourceMappingURL=app.routing.js.map