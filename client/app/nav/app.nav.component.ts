import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  moduleId: module.id.replace("/dist/app/", "/"),
  selector: 'navi',
  templateUrl: 'nav.html',
  styleUrls: ['../styles.css']

})
export class navComponent implements OnInit {
  boolLog : any;
  constructor(
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService) { console.log(this.boolLog); }

     isLoggedIn() {
        return this.authService.isLoggedIn();
    }

  ngOnInit() {
    const log = localStorage.getItem('id_token');
    console.log(log);
    this.boolLog = 1;
    if (log == null) {
            this.boolLog = null;
        }
    
  }

  onLogoutClick() {
    this.authService.logout();
    this.flashMessage.show('You are logged out', {
      cssClass: 'alert-success',
      timeout: 3000
    });
    this.router.navigate(['/login']);
    return false;
  }
}