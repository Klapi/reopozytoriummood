var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var passport = require("passport");
var index = require('./routes/index');
var recipes = require('./routes/recipes');
const mongoose = require('mongoose');
const config = require('./config/database');
const users = require('./routes/users');


// Connect To Database
mongoose.connect(config.database);

// On Connection
mongoose.connection.on('connected', () => {
  console.log('Connected to database ' + config.database);
});

// On Error
mongoose.connection.on('error', (err) => {
  console.log('Database error: ' + err);
});


if(process.env.NODE_ENV=='prod')
{
  console.log('1');
  process.env.URL_API='https://majestic-yosemite-19251.herokuapp.com/api/';
  process.env.URL_USERS='https://majestic-yosemite-19251.herokuapp.com/users/';
}
else
{
  console.log(process.env.NODE_ENV);
  console.log('2');
  process.env.URL_API='http://localhost:3000/api';
  process.env.URL_USERS='http://localhost:3000/users';
}

var port = process.env.PORT || 3000;

var app = express();

//View
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Static folder
app.use(express.static(path.join(__dirname, 'client')));


app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);
//Body pars
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', index);
app.use('/api', recipes);
app.use('/users', users);

//Listening on port 3000
app.listen(port, function () {
  console.log('server started on port' + port);
});

module.exports = app;