const request = require('supertest');
const app = require('../server');
const Users = require('../models/user.js');
var sinon = require('sinon');

describe("Get recipe", function () {
    it("Get recipe", function (done) {
        request("http://localhost:3000/api/")
            .get("recipes/591d5191b01858095764638c")
            .expect(200, done);
    });
});



describe("Post recipe", function () {
    let newRecipe = {
        description: "opis",
        time: "15h",
        ingredients: [
            {
                "name": "jablko",
                "quantity": "",
                "unit": ""
            },
            {
                "name": "cynamon",
                "quantity": "",
                "unit": ""
            }
        ],
        image: "http://www.badania-medyczne.pl/wp-content/uploads/2015/09/warto_jesc_polskie_jablka.jpg?x73780",
        preparing: [
            "1.In a large bowl, sift together the flour, baking powder, salt and sugar. Make a well in the center and pour in the milk, egg and melted butter; mix until smooth.",
            "2.Heat a lightly oiled griddle or frying pan over medium high heat. Pour or scoop the batter onto the griddle, using approximately 1/4 cup for each pancake. Brown on both sides and serve hot."
        ]
    }


    it("Should not post recipe and should res 400 (no title)", function (done) {
        request("http://localhost:3000/api/")
            .post("recipe")
            .send(newRecipe)
            .expect(400, done);
    });

    
    it("Should post recipe and should res 200", function (done) {
        newRecipe["title"] = "jablko";
        request("http://localhost:3000/api/")
            .post("recipe")
            .send(newRecipe)
            .expect(200, done);
    });

});

