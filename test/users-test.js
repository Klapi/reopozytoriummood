var should = require("should");
var request = require("request");
var expect = require("chai").expect;
var baseUrl = "https://swapi.co/api";
var util = require("util");
const express = require('express');
const app = require('../server');

var sinon = require('sinon');
var chai = require('chai');
var expect = chai.expect;
var mongoose = require('mongoose');
require('sinon-mongoose');

//Importing our todo model for our unit testing.
var Todo = require('../models/user');

describe("Get all users", function () {
    // Test will pass if we get all users
    it("should return all users", function (done) {
        var TodoMock = sinon.mock(Todo);
        var expectedResult = { status: true, todo: [] };
        TodoMock.expects('find').yields(null, expectedResult);
        Todo.find(function (err, result) {
            TodoMock.verify();
            TodoMock.restore();
            expect(result.status).to.be.true;
            done();
        });
    });

    // Test will pass if we fail to get a todo
    it("should return error", function (done) {
        var TodoMock = sinon.mock(Todo);
        var expectedResult = { status: false, error: "Something went wrong" };
        TodoMock.expects('find').yields(expectedResult, null);
        Todo.find(function (err, result) {
            TodoMock.verify();
            TodoMock.restore();
            expect(err.status).to.not.be.true;
            done();
        });
    });
});

// describe('returns luke', function () {
//     it('returns luke', function (done) {
//         request.get({ url: baseUrl + '/people/1/' },
//             function (error, response, body) {
//                 var bodyObj = JSON.parse(body);
//                 expect(bodyObj.name).to.equal("Luke Skywalker");
//                 expect(bodyObj.hair_color).to.equal("blond");
//                 expect(response.statusCode).to.equal(200);
//                 console.log(body);
//                 done();
//             });
//     });
// });



// describe('Login API', function () {
//     it('Should success if credential is valid', function (done) {
//         var recipe = {

//             time: '1 min',
//             preparing: 'bułka',
//             title: '',
//             ingredients: 'bułka',
//             image: 'http://www.piekarniadomino.pl/wp-content/uploads/2015/01/bulka_kajzerka.png',
//             description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
//         };
//         request(app)
//             .post('/recipe')
//             .set('Accept', 'application/json')
//             .set('Content-Type', 'application/json')
//             .send({ recipe })
//             .expect(400)
//             .end(done);
//     });
// });