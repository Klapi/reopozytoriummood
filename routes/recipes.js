var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://klapi:klapi@ds139791.mlab.com:39791/zad_list');
const passport = require('passport');


//get all
router.get('/recipes', passport.authenticate('jwt-user', { session: false }), function (req, res) {
    db.recipes.find(function (err, recipes) {
        if (err) {
            res.send(err);
        }
        res.json(recipes);
    });
});

//get all recipes of user_id
router.get('/recipes/:user_id', passport.authenticate('jwt-user', { session: false }), function (req, res) {
    db.recipes.find({ author: req.params.user_id }, function (err, recipes) {
        if (err) {
            res.send(err);
        }
        res.json(recipes);
    });
});

//get single
router.get('/recipe/:id', passport.authenticate('jwt-user', { session: false }), function (req, res, next) {
    db.recipes.findOne({ _id: mongojs.ObjectId(req.params.id) }, function (err, recipe) {
        if (err) {
            res.send(err);
        }
        res.json(recipe);
    });
});

//get last recipe
router.get('/recipes/last', function (req, res, next) {
    db.recipes.find().sort({ $natural: -1 }).limit(1);
});

//Save recipe
router.post('/recipe', function (req, res, next) {
    var recipe = req.body;
    if (!recipe.title) {
        res.status(400);
        res.json({ "error": "Bad data" });
    }
    else {
        db.recipes.save(recipe, function (err, recipe) {
            if (err) {
                res.send(err);
            }
            res.json(recipe);
        });
    }
});

//delete recipe
router.delete('/recipe/:id', function (req, res, next) {
    db.recipes.remove({ _id: mongojs.ObjectId(req.params.id) }, function (err, recipe) {
        if (err) {
            res.send(err);
        }
        res.json(recipe);
    });
});

//update recipe
router.put('/recipe/:id', function (req, res, next) {
    var recipe = req.body;
    var updRecipe = {};


    db.recipes.remove({ _id: mongojs.ObjectId(req.params.id) }, function (err, recipe) {
        if (err) {
            res.send(err);
        }
        res.json(recipe);
    });
});

module.exports = router;